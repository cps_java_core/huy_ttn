import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        //Bai1();
        //Bai2();
        //Bai3();
        //Bai4();
        Bai5();
    
    }

    public static void Bai1() {
        System.out.println("HI ALL! WELLCOME TO CODE P SOFT GROUP!");
    }

    public static void Bai2() {
        double a=0,b=0;
        char c='+';
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập a: ");
        a = sc.nextDouble();
        System.out.print("Nhập b: ");
        b = sc.nextDouble();
        System.out.print("Nhập phép tính thực hiện(+,-,*,/): ");
        c = sc.next().charAt(0);

        switch(c){
            case '+':
            System.out.println("Ket qua: "+(a+b));
            break;
            case '-':
            System.out.println("Ket qua: "+(a-b));
            break;
            case '*':
            System.out.println("Ket qua: "+(a*b));
            break;
            case '/':
            System.out.println("Ket qua: "+(a/b));
            break;
            default:
            System.out.println("Nhap sai");
        }
        sc.close();
    }

    public static void Bai3(){
        int a=0,b=0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập a: ");
        a = sc.nextInt();
        System.out.print("Nhập b: ");
        b = sc.nextInt();
        System.out.println("a^2 + b: "+(a*a+b));
        sc.close();
    }

    public static void Bai4() {
        int a=0,b=0;
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập a: ");
        a = sc.nextInt();
        System.out.print("Nhập b: ");
        b = sc.nextInt();
        System.out.println("a/b= "+a/b);
        sc.close();
    }

    public static void Bai5() {
        double a=0;
        int b=0;
        char c='+';
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhập a: ");
        a = sc.nextDouble();
        System.out.print("Nhập b: ");
        b = sc.nextInt();
        System.out.print("Nhập phép tính thực hiện(+,-,*,/): ");
        c = sc.next().charAt(0);
        
        switch(c){
          case '+':
          System.out.println("Ket qua: "+a+b);
          break;
          case '-':
          System.out.println("Ket qua: "+(a-b));
          break;
          case '*':
          System.out.println("Ket qua: "+a*b);
          break;
          case '/':
          System.out.println("Ket qua: "+a/b);
          break;
        }
        sc.close();
    }
}
